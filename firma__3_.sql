-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 10 Sty 2022, 10:17
-- Wersja serwera: 10.4.21-MariaDB
-- Wersja PHP: 7.3.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `firma`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pracownicy`
--

CREATE TABLE `pracownicy` (
  `id` int(11) NOT NULL,
  `imie` varchar(20) COLLATE utf8mb4_polish_ci NOT NULL,
  `Nazwisko` varchar(30) COLLATE utf8mb4_polish_ci NOT NULL,
  `placa` decimal(7,0) NOT NULL,
  `stanowisko` varchar(20) COLLATE utf8mb4_polish_ci NOT NULL,
  `pesel` char(11) COLLATE utf8mb4_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `pracownicy`
--

INSERT INTO `pracownicy` (`id`, `imie`, `Nazwisko`, `placa`, `stanowisko`, `pesel`) VALUES
(1, 'Adam', 'Kowalski', '1625', 'magazynier', '1234I678901'),
(2, 'Adam', 'Nowak', '3760', 'kierownik', '9234I678901'),
(3, 'Adam', 'Nowak', '2760', 'sprzedawca', '9234I678901'),
(4, 'Krzysztof', 'Malinowski', '1760', 'magazynier', '9234I678901'),
(5, 'Zygmunt', 'Nowicki', '2760', 'magazynier', '9935I677601'),
(6, 'Krzysztof', 'Nowicki', '3760', 'magazynier', '8235I678901'),
(7, 'Kamil', 'Borowski', '2360', 'asystent', '32349678913'),
(8, 'Zygmunt', 'Kwiatkowski', '2560', 'magazynier', '8935I767601'),
(9, 'Krzysztof', 'Arkuszewski', '1601', 'magazynier', '02343678913'),
(10, 'Kacper', 'Adamczyk', '1611', 'serwisant', '92341678903'),
(11, 'Arkadiusz', 'Malinowski', '1600', 'kierowca', '9234I678909'),
(12, 'Andrzej', 'Kowalski', '4200', 'kierownik', '7234I678901'),
(13, 'Kamil', 'Andrzejczak', '1200', 'asystent', ''),
(14, 'Zygmunt', 'Makówka', '2760', 'magazynier', '8854I677601'),
(15, 'Zygmunt', 'Maślanka', '2260', 'sprzedawca', '01032877601'),
(16, 'Kamila', 'Borowska', '2360', 'asystent', '99349678913'),
(17, 'Frajer', 'Pumba', '2500', 'sprzatacz', '5849363523'),
(18, 'Wacław', 'Nowak', '15000', 'kierownik', '12345678915');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `stanowiska`
--

CREATE TABLE `stanowiska` (
  `id` int(100) NOT NULL,
  `nazwa` varchar(150) NOT NULL,
  `placa` decimal(7,0) NOT NULL,
  `dzial` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `stanowiska`
--

INSERT INTO `stanowiska` (`id`, `nazwa`, `placa`, `dzial`) VALUES
(4, 'magazynier', '2800', 'buty'),
(1, 'kierownik', '15000', 'buty'),
(2, 'kierowca', '2800', 'buty'),
(3, 'sprzedawca', '3000', 'buty');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `pracownicy`
--
ALTER TABLE `pracownicy`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
