-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 13 Gru 2021, 12:27
-- Wersja serwera: 10.4.21-MariaDB
-- Wersja PHP: 7.3.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `test`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pytania1`
--

CREATE TABLE `pytania1` (
  `id` int(20) NOT NULL,
  `tresc pytania` text COLLATE utf8_polish_ci NOT NULL,
  `a` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `b` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `c` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `d` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `prawidlowa_odp` varchar(20) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `pytania1`
--

INSERT INTO `pytania1` (`id`, `tresc pytania`, `a`, `b`, `c`, `d`, `prawidlowa_odp`) VALUES
(1, 'Przedstawiony blok reprezentuje czynność ', 'A. zastosowania goto', 'B. wczytania lub wyś', 'C. wykonania zadania', 'D. podjęcia decyzji.', 'D'),
(2, 'Aby zadeklarować pole klasy, do którego mają dostęp jedynie metody tej klasy i pole to nie jest dostępne dla\r\nklas pochodnych, należy użyć kwalifikatora dostępu', 'A. public', 'B. private', 'C. protected', 'D. published', 'B'),
(3, 'Pętla while powinna być wykonywana tak długo, jak długo zmienna x będzie przyjmowała wartości\r\nz przedziału obustronnie otwartego (-2, 5). Zapis tego warunku w nagłówku pętli za pomocą języka PHP ma\r\npostać\r\n', 'A. ($x > -2) && ($x < 5)', 'B. ($x == -2) && ($x < 5)', 'C. ($x < -2) || ($x > 5)', 'D. ($x > -2) || ($x > 5)', 'A'),
(4, 'Którym słowem kluczowym, w języku z rodziny C należy posłużyć się, aby przypisać alternatywną nazwę\r\ndla istniejącego typu danych?\r\n', 'A. enum', 'B. union', 'C. switch', 'D. typedef', 'D'),
(5, 'DOM dostarcza metod i własności, które w języku JavaScript pozwalają na\r\n', 'A. manipulowanie zadeklarowanymi w kodzie łańcuchami.', 'B. wysłanie danych formularza bezpośrednio do bazy danych', 'C. wykonywanie operacji na zmiennych przechowujących liczby.', 'D. pobieranie i modyfikowanie elementów strony wyświetlonej przez przeglądarkę.\r\n', 'D'),
(6, 'Testy dotyczące skalowalności oprogramowania mają za zadanie sprawdzić, czy aplikacja', 'A. ma odpowiednią funkcjonalność.', 'B. jest odpowiednio udokumentowana.\r\n', 'C. potrafi działać przy zakładanym i większym obciążeniu.', 'D. jest zabezpieczona przed niedozwolonymi operacjami, np. dzielenie przez zero.', 'C'),
(7, 'W relacyjnych bazach danych, jeżeli dwie tabele są połączone za pomocą ich kluczy głównych, mamy do\r\nczynienia z relacją\r\n', 'A. 1..1', 'B. 1..n', 'C. n..1', 'D. n..n', 'A'),
(8, 'Wbudowanym w pakiet XAMPP narzędziem służącym do zarządzania bazą danych jest', 'A. MySQL Workbench', 'B. phpMyAdmin', 'C. pgAdmin', 'D. SQLite', 'B'),
(9, 'Funkcją agregującą zwracającą liczbę rekordów jest', 'A. SUM', 'B. AVG', 'C. COUNT', 'D. NUMBER', 'C'),
(10, 'Zapytanie z klauzulą JOIN stosuje się, aby', 'A. wywołać funkcję agregującą', 'B. zdefiniować klucz obcy dla tabeli.', 'C. otrzymać wynik jedynie z jednej tabeli.', 'D. uzyskać wyniki z dwóch tabel pozostających ze sobą w relacji.', 'D');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `pytania1`
--
ALTER TABLE `pytania1`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
