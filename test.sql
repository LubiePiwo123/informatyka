-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 10 Sty 2022, 10:15
-- Wersja serwera: 10.4.21-MariaDB
-- Wersja PHP: 7.3.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `test`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `klient`
--

CREATE TABLE `klient` (
  `id_klient` int(11) NOT NULL,
  `name` varchar(15) DEFAULT NULL,
  `surname` varchar(15) DEFAULT NULL,
  `id_adres` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pytania`
--

CREATE TABLE `pytania` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pytania1`
--

CREATE TABLE `pytania1` (
  `id` int(20) NOT NULL,
  `tresc pytania` text COLLATE utf8_polish_ci NOT NULL,
  `a` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `b` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `c` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `d` varchar(200) COLLATE utf8_polish_ci NOT NULL,
  `prawidlowa_odp` varchar(20) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `pytania1`
--

INSERT INTO `pytania1` (`id`, `tresc pytania`, `a`, `b`, `c`, `d`, `prawidlowa_odp`) VALUES
(1, 'Przedstawiony blok reprezentuje czynność ', 'A. zastosowania goto', 'B. wczytania lub wyś', 'C. wykonania zadania', 'D. podjęcia decyzji.', 'D'),
(2, 'Aby zadeklarować pole klasy, do którego mają dostęp jedynie metody tej klasy i pole to nie jest dostępne dla\r\nklas pochodnych, należy użyć kwalifikatora dostępu', 'A. public', 'B. private', 'C. protected', 'D. published', 'B'),
(3, 'Pętla while powinna być wykonywana tak długo, jak długo zmienna x będzie przyjmowała wartości\r\nz przedziału obustronnie otwartego (-2, 5). Zapis tego warunku w nagłówku pętli za pomocą języka PHP ma\r\npostać\r\n', 'A. ($x > -2) && ($x < 5)', 'B. ($x == -2) && ($x < 5)', 'C. ($x < -2) || ($x > 5)', 'D. ($x > -2) || ($x > 5)', 'A'),
(4, 'Którym słowem kluczowym, w języku z rodziny C należy posłużyć się, aby przypisać alternatywną nazwę\r\ndla istniejącego typu danych?\r\n', 'A. enum', 'B. union', 'C. switch', 'D. typedef', 'D'),
(5, 'DOM dostarcza metod i własności, które w języku JavaScript pozwalają na\r\n', 'A. manipulowanie zadeklarowanymi w kodzie łańcuchami.', 'B. wysłanie danych formularza bezpośrednio do bazy danych', 'C. wykonywanie operacji na zmiennych przechowujących liczby.', 'D. pobieranie i modyfikowanie elementów strony wyświetlonej przez przeglądarkę.\r\n', 'D'),
(6, 'Testy dotyczące skalowalności oprogramowania mają za zadanie sprawdzić, czy aplikacja', 'A. ma odpowiednią funkcjonalność.', 'B. jest odpowiednio udokumentowana.\r\n', 'C. potrafi działać przy zakładanym i większym obciążeniu.', 'D. jest zabezpieczona przed niedozwolonymi operacjami, np. dzielenie przez zero.', 'C'),
(7, 'W relacyjnych bazach danych, jeżeli dwie tabele są połączone za pomocą ich kluczy głównych, mamy do\r\nczynienia z relacją\r\n', 'A. 1..1', 'B. 1..n', 'C. n..1', 'D. n..n', 'A'),
(8, 'Wbudowanym w pakiet XAMPP narzędziem służącym do zarządzania bazą danych jest', 'A. MySQL Workbench', 'B. phpMyAdmin', 'C. pgAdmin', 'D. SQLite', 'B'),
(9, 'Funkcją agregującą zwracającą liczbę rekordów jest', 'A. SUM', 'B. AVG', 'C. COUNT', 'D. NUMBER', 'C'),
(10, 'Zapytanie z klauzulą JOIN stosuje się, aby', 'A. wywołać funkcję agregującą', 'B. zdefiniować klucz obcy dla tabeli.', 'C. otrzymać wynik jedynie z jednej tabeli.', 'D. uzyskać wyniki z dwóch tabel pozostających ze sobą w relacji.', 'D');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `samochody`
--

CREATE TABLE `samochody` (
  `ID` int(11) NOT NULL,
  `marka` varchar(30) DEFAULT NULL,
  `model` varchar(30) DEFAULT NULL,
  `rocznik` int(11) DEFAULT NULL,
  `pojemnosc` int(11) DEFAULT NULL,
  `przyspieszenie` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `samochody`
--

INSERT INTO `samochody` (`ID`, `marka`, `model`, `rocznik`, `pojemnosc`, `przyspieszenie`) VALUES
(1, 'Mercedes_Benz', 'CLA', 2017, 2, 4.5),
(2, 'Mercedes_Benz', 'Klasa E', 2019, 4, 4.5),
(3, 'Mercedes_Benz', 'GLS', 2019, 4, 4.6),
(4, 'Audi', 'e-Tron', 2021, 4, 5),
(5, 'Audi', 'A4', 2015, 4, 3),
(6, 'Audi', 'Quattro 7', 2019, 4, 3),
(7, 'BMW', 'seria 2_Coupe', 2021, 4, 3),
(8, 'BMW', 'seria 3_Touring', 2020, 4, 3),
(9, 'BMW', 'seria 4_Cabrio', 2020, 4, 3),
(10, 'Toyota', 'rx5', 2020, 4, 3);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uprawnienia`
--

CREATE TABLE `uprawnienia` (
  `id` int(11) NOT NULL,
  `uprawnienie` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `uprawnienia`
--

INSERT INTO `uprawnienia` (`id`, `uprawnienie`) VALUES
(1, 3),
(2, 3),
(3, 1),
(4, 4),
(5, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uzytkownicy`
--

CREATE TABLE `uzytkownicy` (
  `id` int(10) NOT NULL,
  `login` varchar(255) NOT NULL,
  `haslo` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `rejestracja` date NOT NULL,
  `logowanie` date NOT NULL,
  `ip` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `uzytkownicy`
--

INSERT INTO `uzytkownicy` (`id`, `login`, `haslo`, `email`, `rejestracja`, `logowanie`, `ip`) VALUES
(1, 'admin', '207023ccb44feb4d7dadca005ce29a64', 'admin@admin.pl', '2021-10-20', '2021-10-20', '127.0.0.1'),
(2, 'klient', 'jdwaihfahalkdhakjdwalkdhwdlawdja', 'klient@admin.pl', '2021-10-20', '2021-10-20', '127.0.0.1');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `klient`
--
ALTER TABLE `klient`
  ADD PRIMARY KEY (`id_klient`);

--
-- Indeksy dla tabeli `pytania`
--
ALTER TABLE `pytania`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `pytania1`
--
ALTER TABLE `pytania1`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `samochody`
--
ALTER TABLE `samochody`
  ADD PRIMARY KEY (`ID`);

--
-- Indeksy dla tabeli `uprawnienia`
--
ALTER TABLE `uprawnienia`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `uzytkownicy`
--
ALTER TABLE `uzytkownicy`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT dla zrzuconych tabel
--

--
-- AUTO_INCREMENT dla tabeli `samochody`
--
ALTER TABLE `samochody`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT dla tabeli `uzytkownicy`
--
ALTER TABLE `uzytkownicy`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
